<?php
namespace Controllers;

use \Core\Controller;
use Models\Logger;
use Models\Users;

class ConfigurationController extends Controller {
    private $user;
    private $dataInfo;
    private $logger;

    public function __construct()
    {
        parent::__construct();
        $this->user = new Users();
        $this->logger = new Logger();

        if (!$this->user->isLogged()) {
            header("Location: ".BASE_URL."login");
            exit;
        }

        $this->dataInfo = [
            'user'       => $this->user,
            'menuActive' => 'home'
        ];
    }

    public function index()
    {
        $this->loadTemplate('configuration', $this->dataInfo);
    }

    public function updateColor()
    {
        $userModel = new Users();

        $color = $_POST['colorPanel'];

        $userModel->updateColor($this->user->getUserId(), $color);

        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'POST');
    }

}