<?php
namespace Controllers;

use \Core\Controller;
use Models\Logger;
use Models\Users;

class LoggerController extends Controller {
    private $user;
    private $dataInfo;
    private $loggerModel;

    public function __construct()
    {
        parent::__construct();
        $this->user = new Users();
        $this->loggerModel = new Logger();

        if (!$this->user->isLogged()) {
            header("Location: ".BASE_URL."login");
            exit;
        }



        $this->dataInfo = [
            'user'       => $this->user,
            'menuActive' => 'logs'
        ];
    }

    public function index()
    {
        $loggerModel = new Logger();

        $logs = $loggerModel->getLogs();

        $this->dataInfo['logInfo'] = $logs;

        $this->loadTemplate('logger', $this->dataInfo);
    }

    public function allJson()
    {
        $loggerModel = new Logger();

        $logs = $loggerModel->getAllLogs();

        $this->dataInfo['logInfo'] = $logs;

        $this->loadView('loggerJson', $this->dataInfo);
    }

    public function register($url, $request, $userId, $type)
    {
        $this->loggerModel->create($url, $request, $userId, $type);
    }

}