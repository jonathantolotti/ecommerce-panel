<?php
namespace Controllers;

use \Core\Controller;
use Models\Logger;
use Models\Pages;
use Models\Users;

class PagesController extends Controller {
    private $user;
    private $dataInfo;
    private $logger;

    public function __construct()
    {
        parent::__construct();
        $this->user = new Users();
        $this->logger = new Logger();

        if (!$this->user->isLogged()) {
            header("Location: ".BASE_URL."login");
            exit;
        }

        if (!$this->user->hasPermission('view-static-page')) {
            header("Location: ".BASE_URL);
        }

        $this->dataInfo = [
            'user' => $this->user,
            'menuActive' => 'pages'
        ];
    }

    public function index()
    {
        $pagesModel = new Pages();

        $this->dataInfo['pagesList'] = $pagesModel->getPages();

        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'GET');

        $this->loadTemplate('pages', $this->dataInfo);
    }

    public function delete($idPage)
    {
        if (empty($idPage)) {
            header("Location: ".BASE_URL.'pages');
        }

        $pagesModel = new Pages();

        $pagesModel->delete($idPage);

        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'DELETE');

        header("Location: ".BASE_URL.'pages');
    }

    public function create()
    {
        $this->dataInfo['errorItems'] = [];

        if (isset($_SESSION['formError']) && count($_SESSION['formError']) > 0) {
            $this->dataInfo['errorItems'] = $_SESSION['formError'];
            unset($_SESSION['formError']);
        }

        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'GET');

        $this->loadTemplate('pages_add', $this->dataInfo);
    }

    public function onCreate()
    {
        if (empty($_POST['title'])) {
            $_SESSION['formError'] = [
                'name'
            ];
            header("Location: ".BASE_URL.'pages/create');
            exit;
        }

        if (empty($_POST['body'])) {
            $_SESSION['formError'] = [
                'body'
            ];
            header("Location: ".BASE_URL.'pages/create');
            exit;
        }

        $pagesModel = new Pages();

        $title = addslashes($_POST['title']);
        $body = addslashes($_POST['body']);

        $pagesModel->addPage($title, $body);

        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'POST');

        header("Location: ".BASE_URL.'pages');


    }

    public function update($idPage)
    {
        if (empty($idPage)) {
            header("Location: ".BASE_URL.'pages');
        }

        $pagesModel = new Pages();

        $this->dataInfo['pageInfo'] = $pagesModel->getPageById($idPage);

        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'GET');

        $this->loadTemplate('pages_update', $this->dataInfo);
    }

    public function onUpdate($idPage)
    {
        if (empty($idPage)) {
            header("Location: ".BASE_URL.'pages');
        }

        if (empty($_POST['title'])) {
            $_SESSION['formError'] = [
                'name'
            ];
            header("Location: ".BASE_URL.'pages');
            exit;
        }

        if (empty($_POST['body'])) {
            $_SESSION['formError'] = [
                'body'
            ];
            header("Location: ".BASE_URL.'pages');
            exit;
        }

        $pagesModel = new Pages();

        $title = addslashes($_POST['title']);
        $body = $_POST['body'];

        $pagesModel->update($idPage, $title, $body);

        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'POST');

        header("Location: ".BASE_URL.'pages');
    }

    public function upload()
    {
        if (!empty($_FILES['file']['tmp_name'])) {
            $typesAllowed = ['image/jpeg', 'image/png'];

            if (in_array($_FILES['file']['type'], $typesAllowed)) {
                $newName = md5(time().rand(0,9999).time()).'jpg';

                if ($_SERVER['REMOTE_ADDR'] == '192.168.0.17') {
                    move_uploaded_file($_FILES['file']['tmp_name'], '../ecommerce/media/pages/'.$newName);
                } else {
                    move_uploaded_file($_FILES['file']['tmp_name'], '../loja.jonathantolotti.com.br/media/pages/'.$newName);
                }

                $linkFile = [
                    'location' => BASE_URL_ECOMMERCE.'media/pages/'.$newName
                ];

                echo json_encode($linkFile);
                exit;
            }

            $this->logger->create($_REQUEST['q'], json_encode($_FILES), $this->user->getUserId(), 'POST');

        }
    }

    public function preview($idPage)
    {
        if (empty($idPage)) {
            header("Location: ".BASE_URL.'pages');
        }

        $pagesModel = new Pages();

        $this->dataInfo['pageInfo'] = $pagesModel->getPageById($idPage);

        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'GET');

        $this->loadView('pages_preview', $this->dataInfo);
    }
}