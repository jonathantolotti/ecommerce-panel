<?php
namespace Controllers;

use \Core\Controller;
use Models\Brands;
use Models\Categories;
use Models\Logger;
use Models\Options;
use Models\Products;
use Models\Users;

class ProductsController extends Controller {
    private $user;
    private $dataInfo;
    private $logger;

    public function __construct()
    {
        parent::__construct();
        $this->user = new Users();
        $this->logger = new Logger();

        if (!$this->user->isLogged()) {
            header("Location: ".BASE_URL."login");
            exit;
        }

        if (!$this->user->hasPermission('view-products')) {
            header("Location: ".BASE_URL);
        }

        $this->dataInfo = [
            'user'       => $this->user,
            'menuActive' => 'products'
        ];
    }

    public function index()
    {
        $productsModel = new Products();

        $this->dataInfo['products'] = $productsModel->getAllProducts();

        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'GET');

        $this->loadTemplate('products', $this->dataInfo);
    }

    public function create()
    {
        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'GET');

        $this->dataInfo['errorItems'] = [];

        $categoriesModel = new Categories();
        $brandsModel = new Brands();
        $optionsModel = new Options();

        $this->dataInfo['categories'] = $categoriesModel->getAllCategories();
        $this->dataInfo['brands'] = $brandsModel->getAllBrands();
        $this->dataInfo['options'] = $optionsModel->getAllOptions();

        if (isset($_SESSION['formError']) && count($_SESSION['formError']) > 0) {
            $this->dataInfo['errorItems'] = $_SESSION['formError'];
            unset($_SESSION['formError']);
        }

        $this->loadTemplate('products_add', $this->dataInfo);
    }

    public function onCreate()
    {
        if (empty($_POST['name'])) {
            header("Location: ".BASE_URL.'products');
            exit;
        }

        $id_category = $_POST['id_category'];
        $id_brand = $_POST['id_brand'];
        $name = $_POST['name'];
        $description = $_POST['description'];
        $stock = $_POST['stock'];
        $price_from = $_POST['price_from'];
        $price = $_POST['price'];
        $weight = $_POST['weight'];
        $width = $_POST['width'];
        $height = $_POST['height'];
        $length = $_POST['length'];
        $diameter = $_POST['diameter'];
        $featured = $_POST['featured'];
        $sale = $_POST['sale'];
        $bestseller = $_POST['bestseller'];
        $new_product = $_POST['new_product'];
        $options = $_POST['options'];

        if (
            !empty($id_category) &&
            !empty($id_brand) &&
            !empty($name) &&
            !empty($stock) &&
            !empty($price)
        ) {
            if ($stock <= 0 ) {
                $_SESSION['formError'] = ['stock'];
                header("Location: ".BASE_URL."products/create");
                exit;
            }

            if ($weight <= 0) {
                $_SESSION['formError'] = ['weight'];
                header("Location: ".BASE_URL."products/create");
                exit;
            }

            if ($width <= 0) {
                $_SESSION['formError'] = ['width'];
                header("Location: ".BASE_URL."products/create");
                exit;
            }

            if ($height <= 0) {
                $_SESSION['formError'] = ['height'];
                header("Location: ".BASE_URL."products/create");
                exit;
            }

            if ($length <= 0) {
                $_SESSION['formError'] = ['length'];
                header("Location: ".BASE_URL."products/create");
                exit;
            }

            if ($diameter <= 0) {
                $_SESSION['formError'] = ['diameter'];
                header("Location: ".BASE_URL."products/create");
                exit;
            }

            $images = (!empty($_FILES['images'])) ? $_FILES['images'] : [];

            $productsModel = new Products();

            $productsModel->addProduct(
                $id_category,
                $id_brand,
                $name,
                $description,
                $stock,
                $price_from,
                $price,
                $weight,
                $width,
                $height,
                $length,
                $diameter,
                $featured,
                $sale,
                $bestseller,
                $new_product,
                $options,
                $images
            );

        } else {
            $_SESSION['formError'] = ['id_category', 'id_brand', 'name', 'stock', 'price'];

            header("Location: ".BASE_URL."products/create");
            exit;
        }

        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'POST');

        header("Location: ".BASE_URL.'products');
    }

    public function update($idProduct)
    {
        if (empty($idProduct)) {
            header("Location: ".BASE_URL.'products');
            exit;
        }

        $this->dataInfo['errorItems'] = [];

        $categoriesModel = new Categories();
        $brandsModel = new Brands();
        $optionsModel = new Options();
        $productsModel = new Products();

        $this->dataInfo['productInfo'] = $productsModel->getProductsInfo($idProduct);
        $this->dataInfo['categories'] = $categoriesModel->getAllCategories();
        $this->dataInfo['brands'] = $brandsModel->getAllBrands();
        $this->dataInfo['options'] = $optionsModel->getAllOptions();

        if (isset($_SESSION['formError']) && count($_SESSION['formError']) > 0) {
            $this->dataInfo['errorItems'] = $_SESSION['formError'];
            unset($_SESSION['formError']);
        }

        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'GET');

        $this->loadTemplate('products_update', $this->dataInfo);
    }

    public function onUpdate($idProduct)
    {
        if (empty($idProduct)) {
            header("Location: ".BASE_URL.'products');
            exit;
        }

        $id_category = $_POST['id_category'];
        $id_brand = $_POST['id_brand'];
        $name = $_POST['name'];
        $description = $_POST['description'];
        $stock = $_POST['stock'];
        $price_from = $_POST['price_from'];
        $price = $_POST['price'];
        $weight = $_POST['weight'];
        $width = $_POST['width'];
        $height = $_POST['height'];
        $length = $_POST['length'];
        $diameter = $_POST['diameter'];
        $featured = $_POST['featured'];
        $sale = $_POST['sale'];
        $bestseller = $_POST['bestseller'];
        $new_product = $_POST['new_product'];
        $options = $_POST['options'];

        if (
            !empty($id_category) &&
            !empty($id_brand) &&
            !empty($name) &&
            !empty($stock) &&
            !empty($price) &&
            !empty($idProduct)
        ) {
            if ($stock <= 0 ) {
                $_SESSION['formError'] = ['stock'];
                header("Location: ".BASE_URL."products/create");
                exit;
            }

            if ($weight <= 0) {
                $_SESSION['formError'] = ['weight'];
                header("Location: ".BASE_URL."products/create");
                exit;
            }

            if ($width <= 0) {
                $_SESSION['formError'] = ['width'];
                header("Location: ".BASE_URL."products/create");
                exit;
            }

            if ($height <= 0) {
                $_SESSION['formError'] = ['height'];
                header("Location: ".BASE_URL."products/create");
                exit;
            }

            if ($length <= 0) {
                $_SESSION['formError'] = ['length'];
                header("Location: ".BASE_URL."products/create");
                exit;
            }

            if ($diameter <= 0) {
                $_SESSION['formError'] = ['diameter'];
                header("Location: ".BASE_URL."products/create");
                exit;
            }

            $c_images = (!empty($_POST['c_images'])) ? $_POST['c_images'] : [];
            $images = (!empty($_FILES['images'])) ? $_FILES['images'] : [];

            $productsModel = new Products();

            $productsModel->update(
                $id_category,
                $id_brand,
                $name,
                $description,
                $stock,
                $price_from,
                $price,
                $weight,
                $width,
                $height,
                $length,
                $diameter,
                $featured,
                $sale,
                $bestseller,
                $new_product,
                $options,
                $images,
                $c_images,
                $idProduct
            );

        } else {
            $_SESSION['formError'] = ['id_category', 'id_brand', 'name', 'stock', 'price'];

            header("Location: ".BASE_URL."products/create");
            exit;
        }

        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'POST');

        header("Location: ".BASE_URL.'products');
    }
}