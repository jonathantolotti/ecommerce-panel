<?php
namespace Controllers;

use \Core\Controller;
use Models\Logger;
use Models\Users;

class HomeController extends Controller {
	private $user;
	private $dataInfo;
	private $logger;

	public function __construct()
    {
        parent::__construct();
		$this->user = new Users();
		$this->logger = new Logger();

		if (!$this->user->isLogged()) {
            header("Location: ".BASE_URL."login");
            exit;
        }

		$this->dataInfo = [
		    'user'       => $this->user,
            'menuActive' => 'home'
        ];
	}

	public function index()
    {
		$this->loadTemplate('home', $this->dataInfo);
	}

}