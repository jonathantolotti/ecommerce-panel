<?php
namespace Controllers;

use \Core\Controller;
use Models\Brands;
use Models\Logger;
use Models\Users;

class BrandsController extends Controller {
    private $user;
    private $dataInfo;
    private $logger;

    public function __construct()
    {
        parent::__construct();
        $this->user = new Users();
        $this->logger = new Logger();

        if (!$this->user->isLogged()) {
            header("Location: ".BASE_URL."login");
            exit;
        }

        if (!$this->user->hasPermission('view-brands')) {
            header("Location: ".BASE_URL);
        }

        $this->dataInfo = [
            'user'       => $this->user,
            'menuActive' => 'brands'
        ];
    }

    public function index()
    {
        $brandsModel = new Brands();

        $this->dataInfo['brands'] = $brandsModel->getAllBrands();

        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'GET');

        $this->loadTemplate('brands', $this->dataInfo);
    }

    public function create()
    {
        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'GET');

        $this->loadTemplate('brands_add', $this->dataInfo);
    }

    public function onCreate()
    {
        if (empty($_POST['name'])) {
            header("Location: ".BASE_URL.'brands');
            exit;
        }

        $name = addslashes($_POST['name']);

        $brandsModel = new Brands();

        $brandsModel->addBrand($name);

        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'POST');

        header("Location: ".BASE_URL.'brands');
    }

    public function delete($idBrand)
    {
        if (empty($idBrand)) {
            header("Location: ".BASE_URL.'brands');
            exit;
        }

        $brandModel = new Brands();

        $brandModel->delete($idBrand);

        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'DELETE');

        header("Location: ".BASE_URL.'brands');
    }

    public function update($idBrand)
    {
        if (empty($idBrand)) {
            header("Location: ".BASE_URL.'brands');
            exit;
        }

        $brandModel = new Brands();

        $this->dataInfo['brandInfo'] = $brandModel->getBrandInfo($idBrand);

        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'GET');

        $this->loadTemplate('brands_update', $this->dataInfo);
    }

    public function onUpdate($idBrand)
    {
        if (empty($idBrand)) {
            header("Location: ".BASE_URL.'brands');
            exit;
        }

        $name = addslashes($_POST['name']);

        $brandModel = new Brands();

        $brandModel->update($name, $idBrand);

        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'POST');

        header("Location: ".BASE_URL.'brands');

    }

}