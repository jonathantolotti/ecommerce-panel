<?php
namespace Controllers;

use \Core\Controller;
use Models\Brands;
use Models\Logger;
use Models\Options;
use Models\Users;

class OptionsController extends Controller {
    private $user;
    private $dataInfo;
    private $logger;

    public function __construct()
    {
        parent::__construct();
        $this->user = new Users();
        $this->logger = new Logger();

        if (!$this->user->isLogged()) {
            header("Location: ".BASE_URL."login");
            exit;
        }

        if (!$this->user->hasPermission('view-options')) {
            header("Location: ".BASE_URL);
        }

        $this->dataInfo = [
            'user'       => $this->user,
            'menuActive' => 'options'
        ];
    }

    public function index()
    {
        $optionsModel = new Options();

        $this->dataInfo['options'] = $optionsModel->getAllOptions(true);

        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'GET');

        $this->loadTemplate('options', $this->dataInfo);
    }

    public function create()
    {
        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'GET');

        $this->loadTemplate('options_add', $this->dataInfo);
    }

    public function onCreate()
    {
        if (empty($_POST['name'])) {
            header("Location: ".BASE_URL.'options');
            exit;
        }

        $name = addslashes($_POST['name']);

        $optionsModel = new Options();

        $optionsModel->addOption($name);

        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'POST');

        header("Location: ".BASE_URL.'options');
    }

    public function delete($idOption)
    {
        if (empty($idOption)) {
            header("Location: ".BASE_URL.'options');
            exit;
        }

        $optionsModel = new Options();

        $optionsModel->delete($idOption);

        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'DELETE');

        header("Location: ".BASE_URL.'options');
    }

    public function update($idOption)
    {
        if (empty($idOption)) {
            header("Location: ".BASE_URL.'options');
            exit;
        }

        $optionsModel = new Options();

        $this->dataInfo['optionInfo'] = $optionsModel->getOptionInfo($idOption);

        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'GET');

        $this->loadTemplate('options_update', $this->dataInfo);
    }

    public function onUpdate($idOption)
    {
        if (empty($idOption)) {
            header("Location: ".BASE_URL.'options');
            exit;
        }

        $name = addslashes($_POST['name']);

        $optionsModel = new Options();

        $optionsModel->update($name, $idOption);

        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'POST');

        header("Location: ".BASE_URL.'options');

    }

}