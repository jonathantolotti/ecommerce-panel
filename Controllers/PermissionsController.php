<?php
namespace Controllers;

use \Core\Controller;
use Models\Logger;
use Models\Permissions;
use Models\Users;

class PermissionsController extends Controller {
    private $user;
    private $dataInfo;
    private $logger;

    public function __construct()
    {
        parent::__construct();
        $this->user = new Users();
        $this->logger = new Logger();

        if (!$this->user->isLogged()) {
            header("Location: ".BASE_URL."login");
            exit;
        }

        if (!$this->user->hasPermission('view-permissions')) {
            header("Location: ".BASE_URL);
        }

        $this->dataInfo = [
            'user' => $this->user,
            'menuActive' => 'permissions'
        ];
    }

    public function index()
    {
        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'GET');
        $permissions = new Permissions();

        $this->dataInfo['permissionList'] = $permissions->getAllGroups();

        $this->loadTemplate('permissions', $this->dataInfo);
    }

    public function delete($idGroup)
    {
        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'DELETE');

        $permission = new Permissions();

        $permission->deleteGroup($idGroup);

        header("Location: ".BASE_URL.'permissions');
        exit;
    }

    public function create()
    {
        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'GET');

        $permissions = new Permissions();

        $this->dataInfo['permission_items'] = $permissions->getAllPermissions();

        $this->loadTemplate('permissions_add', $this->dataInfo);
    }

    public function createItem()
    {
        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'GET');

        $this->loadTemplate('permission_item_add', $this->dataInfo);
    }

    public function onCreateItem()
    {
        if (empty($_POST['name']) || empty($_POST['slug'])) {
            header("Location: ".BASE_URL.'permissions');
            exit;
        }

        $name = addslashes($_POST['name']);
        $slug = addslashes($_POST['slug']);

        $permissionModel = new Permissions();

        $permissionModel->addPermissionItem($name, $slug);

        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'POST');

        header("Location: ".BASE_URL.'permissions');

    }

    public function onCreate()
    {
        $permissions = new Permissions();

        if (!empty($_POST['name'])) {
            $name = addslashes($_POST['name']);

            $idGroup = $permissions->addGroup($name);

            if (
                isset($_POST['items']) &&
                count($_POST['items']) > 0
            ) {
                $items = $_POST['items'];

                foreach ($items as $item) {
                    $permissions->linkItemToGroup($item, $idGroup);
                }

                $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'POST');

                header("Location: ".BASE_URL.'permissions');
                exit;

            } else {
                header("Location: ".BASE_URL.'permissions/create');
                exit;
            }
        }
    }

    public function update($idGroup)
    {
        if (empty($idGroup)) {
            header("Location: ".BASE_URL.'permissions');
            exit;
        }

        $permissions = new Permissions();

        $this->dataInfo = [
            'user'                   => $this->user,
            'permission_items'       => $permissions->getAllPermissions(),
            'permission_id'          => $idGroup,
            'permission_group_name'  => $permissions->getPermissionGroupName($idGroup),
            'permission_group_slugs' => $permissions->getPermissions($idGroup),
            'menuActive'             => 'permissions'
        ];

        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'GET');

        $this->loadTemplate('permissions_update', $this->dataInfo);
    }

    public function onUpdate($idGroup)
    {
        if (empty($idGroup)) {
            header("Location: ".BASE_URL.'permissions');
            exit;
        }

        $permissions = new Permissions();

        if (
            !empty($_POST['name'])&&
            isset($_POST['items']) &&
            count($_POST['items']) > 0
        ) {
            $name = $_POST['name'];
            $items = $_POST['items'];

            $permissions->setName($name, $idGroup);
            $permissions->clearLink($idGroup);

            foreach ($items as $item) {
                $permissions->linkItemToGroup($item, $idGroup);
            }

            $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'POST');

            header("Location: ".BASE_URL.'permissions/update/'.$idGroup);
            exit;

        } else {
            header("Location: ".BASE_URL.'permissions/create');
            exit;
        }
    }
}