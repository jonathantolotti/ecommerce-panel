<?php
namespace Controllers;

use \Core\Controller;
use Models\Users;

class LoginController extends Controller {

    public function index()
    {
        $data = [
            'error' => ''
        ];

        if (!empty($_SESSION['errorMsg'])) {
            $data['error'] = $_SESSION['errorMsg'];
            $_SESSION['errorMsg'] = '';
        }
        $this->loadView('login', $data);
    }

    public function auth()
    {
        if (!empty($_POST['email']) && !empty($_POST['password'])) {
            $email = addslashes($_POST['email']);
            $password = addslashes(md5($_POST['password']));

            $user = new Users();

            if ($user->validateAuth($email,$password)) {
                header("Location: ".BASE_URL);
                exit;
            } else {
                $_SESSION['errorMsg'] = 'Usuário ou senha inválidos';

            }
        } else {
            $_SESSION['errorMsg'] = 'Preencha todos os campos.';

        }
        header("Location: ".BASE_URL."login");
        exit;
    }

    public function logout()
    {
        unset($_SESSION['token']);
        header("Location: ". BASE_URL);
        exit;
    }

}