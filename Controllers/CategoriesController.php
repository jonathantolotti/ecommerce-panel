<?php
namespace Controllers;

use \Core\Controller;
use Models\Logger;
use Models\Users;
use Models\Categories;

class CategoriesController extends Controller {
    private $user;
    private $dataInfo;
    private $logger;

    public function __construct()
    {
        parent::__construct();
        $this->user = new Users();
        $this->logger = new Logger();

        if (!$this->user->isLogged()) {
            header("Location: ".BASE_URL."login");
            exit;
        }

        if (!$this->user->hasPermission('view-categories')) {
            header("Location: ".BASE_URL);
        }

        $this->dataInfo = [
            'user'       => $this->user,
            'menuActive' => 'categories'
        ];
    }

    public function index()
    {
        $categoryModel = new Categories();

        $this->dataInfo['categories'] = $categoryModel->getAllCategories();

        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'GET');

        $this->loadTemplate('categories', $this->dataInfo);
    }

    public function create()
    {
        $categoryModel = new Categories();

        $this->dataInfo['categories'] = $categoryModel->getAllCategories();
        $this->dataInfo['errorItems'] = [];

        if (isset($_SESSION['formError']) && count($_SESSION['formError']) > 0) {
            $this->dataInfo['errorItems'] = $_SESSION['formError'];
            unset($_SESSION['formError']);
        }

        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'GET');

        $this->loadTemplate('categories_add', $this->dataInfo);
    }

    public function onCreate()
    {
        if (!empty($_POST['name'])) {

            $name = addslashes($_POST['name']);
            $sub = '';

            if (!empty($_POST['sub'])) {
                $sub = addslashes($_POST['sub']);
            }

            $categoryModel = new Categories();

            $categoryModel->addCategory($name, $sub);

            $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'POST');

            header("Location: ".BASE_URL.'categories');
            exit;

        } else {
            $_SESSION['formError'] = [
                'name'
            ];
            header("Location: ".BASE_URL.'categories/create');
            exit;
        }
    }

    public function update($idCategory)
    {
        if (empty($idCategory)) {
            header("Location:" . BASE_URL . 'categories');
            exit;
        }

            $categoryModel = new Categories();

            $this->dataInfo['categories'] = $categoryModel->getAllCategories();
            $this->dataInfo['errorItems'] = [];
            $this->dataInfo['categoryInfo'] = $categoryModel->getCategoryInfoById($idCategory);
            $this->dataInfo['idCategory'] = $idCategory;

            if (count($this->dataInfo['categoryInfo']) <= 0) {
                header("Location:" . BASE_URL . 'categories');
                exit;
            }

                if (isset($_SESSION['formError']) && count($_SESSION['formError']) > 0) {
                    $this->dataInfo['errorItems'] = $_SESSION['formError'];
                    unset($_SESSION['formError']);
                }

        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'GET');

        $this->loadTemplate('categories_update', $this->dataInfo);
    }

    public function onUpdate($idCategory)
    {
        if (empty($idCategory)) {
            header("Location:" . BASE_URL . 'categories/update/'.$idCategory);
            exit;
        }

        if (empty($_POST['name'])) {
            $_SESSION['formError'] = [
                'name'
            ];
            header("Location: ".BASE_URL.'categories/update'.$idCategory);
            exit;
        }

        $name = addslashes($_POST['name']);
        $sub = '';

        if (!empty($_POST['sub'])) {
            $sub = addslashes($_POST['sub']);
        }

        $categoryModel = new Categories();

        $categoryModel->updateCategory($name, $sub, $idCategory);

        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'POST');

        header("Location:". BASE_URL.'categories');

    }

    public function delete($idCategory)
    {
        if (empty($idCategory)) {
            header("Location:" . BASE_URL . 'categories/update/'.$idCategory);
            exit;
        }

        $categoryModel = new Categories();

        $categories = $categoryModel->scanCategory($idCategory);

        if ($categoryModel->hasProducts($categories) == false) {
            $categoryModel->deleteCategories($categories);
        }

        $this->logger->create($_REQUEST['q'], json_encode($_POST), $this->user->getUserId(), 'DELETE');

        header("Location: ".BASE_URL.'categories');
        exit;
    }

}