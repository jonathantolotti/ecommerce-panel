<?php
namespace Controllers;

use \Core\Controller;
use Models\Users;

class NotFoundController extends Controller {
    private $user;

    public function __construct()
    {
        parent::__construct();
        $this->user = new Users();

        if (!$this->user->isLogged()) {
            header("Location: ".BASE_URL."login");
            exit;
        }
    }

    public function index()
    {
        $data = [
            'user'       => $this->user,
            'menuActive' => ''
        ];

        $this->loadTemplate('404', $data);
    }

}