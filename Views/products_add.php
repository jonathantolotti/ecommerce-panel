<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Produtos
        <small>Gerenciamento de produtos</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo BASE_URL;?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="<?php echo BASE_URL;?>products"><i class="fa fa-unlock"></i>Produtos</a></li>
        <li class="active">Novo produto</li>
    </ol>

</section>

<!-- Main content -->
<section class="content container-fluid">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Novo produto</h3>
            <div class="box-tools">

            </div>
        </div>
        <div class="box-body">
            <form method="POST" action="<?php echo BASE_URL;?>products/onCreate" enctype="multipart/form-data">

                <div class="form-group" <?php echo (in_array('id_category', $errorItems)) ? 'has-error' : ''; ?>
                <div class="form-group">
                    <label for="category">Categoria:</label>
                    <select name="id_category" id="category" required class="form-control">
                            <?php $this->loadView('categories_add_items', [
                                'categories' => $categories,
                                'level'      => 0,
                                'selected' => ''
                            ]); ?>
                    </select>
                </div>

                <div class="form-group" <?php echo (in_array('id_brand', $errorItems)) ? 'has-error' : ''; ?>
                <div class="form-group">
                    <label for="brand">Marca:</label>
                    <select name="id_brand" id="brand" required class="form-control">
                        <?php foreach ($brands as $brand): ?>
                        <option value="<?php echo $brand['id']; ?>"><?php echo $brand['name'];?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="form-group" <?php echo (in_array('name', $errorItems)) ? 'has-error' : ''; ?>
                <div class="form-group">
                    <label for="name">Título do produto:</label>
                    <input type="text" name="name" id="name" class="form-control" required>
                </div>

                <div class="form-group" <?php echo (in_array('description', $errorItems)) ? 'has-error' : ''; ?>
                <div class="form-group">
                    <label for="description">Descrição do produto:</label>
                    <textarea id="description" name="description"></textarea>
                </div>

                <div class="form-group" <?php echo (in_array('stock', $errorItems)) ? 'has-error' : ''; ?>
                <div class="form-group">
                    <label for="stock">Estoque:</label>
                    <input type="number" name="stock" id="stock" class="form-control" required>
                </div>

                <div class="form-group" <?php echo (in_array('price_from', $errorItems)) ? 'has-error' : ''; ?>
                <div class="form-group">
                    <label for="price_from">Preço de:</label>
                    <input type="text" name="price_from" id="price_from" class="form-control">
                </div>

                <div class="form-group" <?php echo (in_array('price', $errorItems)) ? 'has-error' : ''; ?>
                <div class="form-group">
                    <label for="price">Preço por:</label>
                    <input type="text" name="price" id="price" class="form-control" required>
                </div>

                <hr>

                <div class="form-group" <?php echo (in_array('weight', $errorItems)) ? 'has-error' : ''; ?>
                <div class="form-group">
                    <label for="weight">Peso (em Kg):</label>
                    <input type="number" name="weight" id="weight" class="form-control">
                </div>

                <div class="form-group" <?php echo (in_array('width', $errorItems)) ? 'has-error' : ''; ?>
                <div class="form-group">
                    <label for="width">Largura (em Cm):</label>
                    <input type="number" name="width" id="width" class="form-control">
                </div>

                <div class="form-group" <?php echo (in_array('height', $errorItems)) ? 'has-error' : ''; ?>
                <div class="form-group">
                    <label for="height">Altura (em Cm):</label>
                    <input type="number" name="height" id="height" class="form-control">
                </div>

                <div class="form-group" <?php echo (in_array('length', $errorItems)) ? 'has-error' : ''; ?>
                <div class="form-group">
                    <label for="lenght">Comprimento (em Cm):</label>
                    <input type="number" name="length" id="lenght" class="form-control">
                </div>

                <div class="form-group" <?php echo (in_array('diameter', $errorItems)) ? 'has-error' : ''; ?>
                <div class="form-group">
                    <label for="diameter">Diametro (em Cm):</label>
                    <input type="number" name="diameter" id="diameter" class="form-control">
                </div>

                <hr>

                <div class="form-group" <?php echo (in_array('featured', $errorItems)) ? 'has-error' : ''; ?>
                <div class="form-group">
                    <label for="featured">Destaque:</label>
                    <select name="featured" id="featured" class="form-control">
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>

                <div class="form-group" <?php echo (in_array('sale', $errorItems)) ? 'has-error' : ''; ?>
                <div class="form-group">
                    <label for="sale">Promoção:</label>
                    <select name="sale" id="sale" class="form-control">
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>

                <div class="form-group" <?php echo (in_array('bestseller', $errorItems)) ? 'has-error' : ''; ?>
                <div class="form-group">
                    <label for="bestseller">Mais Vendidos:</label>
                    <select name="bestseller" id="bestseller" class="form-control">
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>

                <div class="form-group" <?php echo (in_array('new_product', $errorItems)) ? 'has-error' : ''; ?>
                <div class="form-group">
                    <label for="newproduct">Novo Produto</label>
                    <select name="new_product" id="newproduct" class="form-control">
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>

                <hr>
                <div class="alert alert-info">
                    <span>Caso não utilize todas as opções, basta deixar sem preenchimento!</span>
                </div>

                <?php foreach ($options as $option): ?>
                    <div class="form-group">
                        <label for="option_<?php echo $option['id']; ?>"><?php echo $option['name']; ?></label>
                        <input type="text" class="form-control" name="options[<?php echo $option['id']; ?>]" id="option_<?php echo $option['id']; ?>">
                    </div>
                <?php endforeach; ?>

                <hr>

                <label for="images">Imagens do produto:</label><br>
                <button class="btn btn-primary newimage">+</button><br>
                <div class="products_files_area">
                    <input type="file" name="images[]" id="images">
                </div>

                <br>
                <input type="submit" class="btn btn-success" value="Salvar">
                <input type="reset" class="btn btn-primary" value="Limpar">
            </form>
        </div>
    </div>

</section>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=yv7wti8c3qtcne0y5a0ez3pmh499q15lf11xjzi89l5k7pzt"></script>
<script type="text/javascript">
    tinymce.init({
        selector: '#description',
        height: 200,
        menubar: false,
        plugins:[
            'textcolor media image lists'
        ],
        toolbar: 'undo redo | formatselect | bold italic backcolor | media image | alignleft aligncenter alignright alignjustify |' +
            ' bulllist numlist | removeformat'

    });
</script>