<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Categorias
        <small>Gerenciamento de categorias e subcategorias</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo BASE_URL;?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Categorias</li>
    </ol>

</section>

<!-- Main content -->
<section class="content container-fluid">

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Categorias</h3>
            <div class="box-tools">
                <a href="<?php echo BASE_URL.'categories/create';?>" class="btn btn-success btn-sm">Nova categoria</a>
            </div>
        </div>
        <div class="box-body">
            <table class="table table-responsive">
                <tr>
                    <th>Nome da categoria</th>
                    <th style="width: 130px;">Ações</th>
                </tr>

            <?php $this->loadView('categories_item', [
                'categories' => $categories,
                'level'      => 0
            ]); ?>

            </table>
        </div>
    </div>

</section>