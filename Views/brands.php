<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Marcas
        <small>Gerenciamento marcas</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo BASE_URL;?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Marcas</li>
    </ol>

</section>

<!-- Main content -->
<section class="content container-fluid">
    <div class="alert alert-warning alert-dismissible">
        <span class="text-bold">Atenção!</span><span> Marcas que estão atreladas em um produto não poderão ser removidas.</span>
    </div>

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Marcas</h3>
            <div class="box-tools">
                <a href="<?php echo BASE_URL.'brands/create';?>" class="btn btn-success btn-sm">Nova marca</a>
            </div>
        </div>
        <div class="box-body">
            <table class="table table-responsive">
                <tr>
                    <th class="text-center">Nome </th>
                    <th class="text-center">Qtd produtos</th>
                    <th style="width: 130px;" class="text-center">Ações</th>
                </tr>

                <?php foreach ($brands as $brand): ?>

                <tr class="text-center">
                    <td>
                        <?php echo $brand['name']; ?>
                    </td>
                    <td>
                        <?php echo $brand['totalProducts']; ?>
                    </td>
                    <td>
                        <div class="btn-group">
                            <a href="<?php echo BASE_URL.'brands/update/'.$brand['id'];?>"
                               class="btn btn-primary btn-xs">Editar</a>
                            <a href="<?php echo BASE_URL.'brands/delete/'.$brand['id'];?>"
                               class="btn btn-danger btn-xs
                                <?php echo ($brand['totalProducts'] != '0')?'disabled': ''; ?>">Excluir</a>
                        </div>
                    </td>
                </tr>

                <?php endforeach; ?>
            </table>
        </div>
    </div>

</section>