<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Opções de produtos
        <small>Gerenciamento de opções</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo BASE_URL;?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Opções de produtos</li>
    </ol>
</section>


<!-- Main content -->
<section class="content container-fluid">

    <div class="alert alert-warning alert-dismissible">
        <span class="text-bold">Atenção!</span><span> Opções que estão atreladas em um produto não poderão ser removidas.</span>
    </div>

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Opções de produtos</h3>
            <div class="box-tools">
                <a href="<?php echo BASE_URL.'options/create';?>" class="btn btn-success btn-sm">Nova opção</a>
            </div>
        </div>
        <div class="box-body">
            <table class="table table-responsive">
                <tr>
                    <th class="text-center">Nome </th>
                    <th style="width: 130px;" class="text-center">Ações</th>
                </tr>

                <?php foreach ($options as $option): ?>

                    <tr class="text-center">
                        <td>
                            <?php echo $option['name']; ?>
                        </td>
                        <td>
                            <div class="btn-group">
                                <a href="<?php echo BASE_URL.'options/update/'.$option['id'];?>"
                                   class="btn btn-primary btn-xs">Editar</a>
                                <a href="<?php echo BASE_URL.'options/delete/'.$option['id'];?>"
                                   class="btn btn-danger btn-xs
                                <?php echo ($option['totalProducts'] != '0')?'disabled': ''; ?>">Excluir</a>
                            </div>
                        </td>
                    </tr>

                <?php endforeach; ?>
            </table>
        </div>
    </div>

</section>