<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Marcas
        <small>Cadastrar nova marca</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo BASE_URL;?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="<?php echo BASE_URL;?>brands"><i class="fa fa-unlock"></i>Marcas</a></li>
        <li class="active">Nova marca</li>
    </ol>

</section>

<!-- Main content -->
<section class="content container-fluid">

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Nova marca</h3>
            <div class="box-tools">

            </div>
        </div>
        <div class="box-body">
            <form method="POST" action="<?php echo BASE_URL;?>brands/onCreate">
                <div class="form-group">
                    <label for="name">Nome:</label>
                    <input type="text" class="form-control" name="name" id="name" required>
                </div>

                <input type="submit" class="btn btn-success" value="Salvar">
                <input type="reset" class="btn btn-primary" value="Limpar">
            </form>
        </div>
    </div>

</section>