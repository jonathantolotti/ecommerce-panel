<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Permissões
        <small>Gerenciamento do nível de acesso dos usuários</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo BASE_URL;?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Permissões</li>
    </ol>

</section>

<!-- Main content -->
<section class="content container-fluid">

    <div class="alert alert-warning alert-dismissible">
        <span class="text-bold">Atenção!</span><span> Permissões que estão atreladas em um usuário não poderão ser removidas.</span>
    </div>

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Grupos de permissões</h3>
            <div class="box-tools">
                <?php if ($viewData['user']->getUserGroup() == 'Desenvolvedor'): ?>
                <a href="<?php echo BASE_URL.'permissions/createItem';?>" class="btn btn-info btn-sm">Novo tipo de permissão</a>
                <?php endif; ?>
                <a href="<?php echo BASE_URL.'permissions/create';?>" class="btn btn-success btn-sm">Novo grupo</a>
            </div>
        </div>
        <div class="box-body">
            <table class="table text-center table-responsive">
                <tr class="text-center">
                    <th>Nome</th>
                    <th>Qtd ativos</th>
                    <th>Ações</th>
                </tr>

                <?php foreach ($permissionList as $permission): ?>
                    <tr>
                        <td><?php echo $permission['name']; ?></td>
                        <td><?php echo $permission['totalUsers']; ?></td>
                        <td>
                            <div class="btn-group">
                                <a href="<?php echo BASE_URL.'permissions/update/'.$permission['id'];?>"
                                   class="btn btn-primary btn-xs">Editar</a>
                                <a href="<?php echo BASE_URL.'permissions/delete/'.$permission['id'];?>"
                                   class="btn btn-danger btn-xs
                                <?php echo ($permission['totalUsers'] != '0')?'disabled': ''; ?>">Excluir</a>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>

            </table>
        </div>
    </div>

</section>