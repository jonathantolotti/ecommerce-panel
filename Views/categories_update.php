<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Categorias
        <small>Gerenciamento de categorias</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo BASE_URL;?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="<?php echo BASE_URL;?>categories"><i class="fa fa-unlock"></i>Categorias</a></li>
        <li class="active">Editar categoria</li>
    </ol>

</section>

<!-- Main content -->
<section class="content container-fluid">

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Editar categoria</h3>
            <div class="box-tools">

            </div>
        </div>
        <div class="box-body">
            <form method="POST" action="<?php echo BASE_URL;?>categories/onUpdate/<?php echo $idCategory; ?>">
                <div class="form-group" <?php echo (in_array('name', $errorItems)) ? 'has-error' : ''; ?>
                <div class="form-group">
                    <label for="name">Nome:</label>
                    <input type="text" class="form-control" name="name" id="name" value="<?php echo $categoryInfo['name']; ?>" required>
                </div>

                <label for="sub">Categoria pai</label>
                <select name="sub" id="sub" class="form-control">
                    <option value="">Nenhuma</option>
                    <?php $this->loadView('categories_add_items', [
                        'categories' => $categories,
                        'level'      => 0,
                        'selected'   =>$categoryInfo['sub']
                    ]); ?>
                </select>
                <br>
                <input type="submit" class="btn btn-success" value="Salvar">
                <input type="reset" class="btn btn-primary" value="Limpar">
            </form>
        </div>
    </div>

</section>