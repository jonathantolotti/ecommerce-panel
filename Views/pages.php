<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Páginas estáticas
        <small>Gerenciamento de páginas estáticas</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo BASE_URL;?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Páginas estáticas</li>
    </ol>

</section>

<!-- Main content -->
<section class="content container-fluid">

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Páginas estáticas</h3>
            <div class="box-tools">
                <a href="<?php echo BASE_URL.'pages/create';?>" class="btn btn-success btn-sm">Nova página</a>
            </div>
        </div>

        <div class="box-body">
            <table class="table text-center table-responsive">
                <tr class="text-center">
                    <th>Nome</th>
                    <th>Preview</th>
                    <th style="width: 130px;">Ações</th>
                </tr>

                <?php foreach ($pagesList as $page): ?>
                    <tr>
                        <td><?php echo $page['title']; ?></td>
                        <td><a href="<?php echo BASE_URL.'pages/preview/'.$page['id'];?>" target="_blank">Visualizar</a> </td>
                        <td>
                            <div class="btn-group">
                                <a href="<?php echo BASE_URL.'pages/update/'.$page['id'];?>"
                                   class="btn btn-primary btn-xs">Editar</a>
                                <a href="<?php echo BASE_URL.'pages/delete/'.$page['id'];?>"
                                   class="btn btn-danger btn-xs">Excluir</a>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>

            </table>
        </div>

    </div>

</section>