<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Configurações do Painel
        <small>Gerenciamento de configurações</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo BASE_URL;?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Configurações</li>
    </ol>
</section>


<!-- Main content -->
<section class="content container-fluid">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Configurações</h3>
        </div>
        <div class="box-body">
            <span>Cor do painel:</span>
            <select id="colorPanel" name="colorPanel">
                <option>Selecione</option>
                <option value="skin-blue" <?php echo $viewData['user']->getColorPanel() == 'skin-blue' ? 'selected':'' ?> >Azul</option>
                <option value="skin-black" <?php echo $viewData['user']->getColorPanel() == 'skin-black' ? 'selected':'' ?>>Preto</option>
                <option value="skin-purple" <?php echo $viewData['user']->getColorPanel() == 'skin-purple' ? 'selected':'' ?>>Roxo</option>
                <option value="skin-yellow" <?php echo $viewData['user']->getColorPanel() == 'skin-yellow' ? 'selected':'' ?>>Amarelo</option>
                <option value="skin-red" <?php echo $viewData['user']->getColorPanel() == 'skin-red' ? 'selected':'' ?>>Vermelho</option>
                <option value="skin-green" <?php echo $viewData['user']->getColorPanel() == 'skin-green' ? 'selected':'' ?>>Verde</option>
            </select>

</section>