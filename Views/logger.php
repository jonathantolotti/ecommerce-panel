<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Logs
        <small>Visualização de logs do sistema</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo BASE_URL;?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Logs</li>
    </ol>

</section>

<section class="content-header">
    <div>
        <a href="<?php echo BASE_URL.'logger/allJson'; ?>" class="btn bg-purple btn-flat margin" target="_blank">JSON de todos logs</a>
    </div>
</section>

<!-- Main content -->
<section class="content container-fluid">
    <div class="box-body">
        <div class="box-group" id="accordion">
            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
            <?php foreach($logInfo as $log): ?>
                <?php if($log['type'] == 'GET'): ?>
                    <div class="panel box box-success">
                <?php elseif ($log['type'] == 'POST'): ?>
                    <div class="panel box box-warning">
                <?php else: ?>
                    <div class="panel box box-danger">
                <?php endif; ?>
                    <div class="box-header with-border">
                        <h4 class="box-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne-<?php echo $log['id']; ?>">
                                <?php echo $log['id'].' - '.$log['url'].' - '.
                                    date('d-m-Y H:i:s',strtotime($log['date'])).' - '.$log['type']; ?>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne-<?php echo $log['id']; ?>" class="panel-collapse collapse">
                        <div class="box-body">
                            <span>Usuário: <?php echo $log['name']; ?></span><br>
                            <span>Request: </span><br>
                            <span>
                                <?php echo '<pre>'; print_r(json_decode($log['request'], true)); echo '</pre>'; ?>
                            </span>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>