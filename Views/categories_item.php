<?php foreach ($categories as $category): ?>
    <tr>
        <td>
            <?php
            for ($q = 0; $q < $level; $q++) echo '-- ';
            echo $category['name'];
            ?>
        </td>
        <td>
            <div class="btn-group">
                <a href="<?php echo BASE_URL.'categories/update/'.$category['id'];?>"
                   class="btn btn-primary btn-xs">Editar</a>
                <a href="<?php echo BASE_URL.'categories/delete/'.$category['id'];?>"
                   class="btn btn-danger btn-xs">Excluir</a>
            </div>
        </td>
    </tr>

    <?php
        if (count($category['subs']) > 0) {
            $this->loadView('categories_item', [
                'categories' => $category['subs'],
                'level'      => $level + 1
            ]);
        }
    ?>

<?php endforeach; ?>