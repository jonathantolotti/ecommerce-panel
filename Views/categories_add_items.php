<?php foreach ($categories as $category): ?>
<?php if ($selected == null) {
        $selected = '';
    }
?>
    <option <?php echo($category['id'] == $selected) ? 'selected="selected"' : ''; ?>
        value="<?php echo $category['id']; ?>">
        <?php
        for ($q = 0; $q < $level; $q++) echo '> ';
        echo $category['name'];
        ?>
    </option>

    <?php
    if (count($category['subs']) > 0) {
        $this->loadView('categories_add_items', [
            'categories' => $category['subs'],
            'level'      => $level + 1,
            'selected'   => $selected
        ]);
    }
    ?>

<?php endforeach; ?>