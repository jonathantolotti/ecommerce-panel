<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Produtos
        <small>Gerenciamento de produtos</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo BASE_URL;?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Produtos</li>
    </ol>

</section>

<!-- Main content -->
<section class="content container-fluid">

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Produtos</h3>
            <div class="box-tools">
                <?php if($viewData['user']->hasPermission('view-options')): ?>
                <a href="<?php echo BASE_URL.'options';?>" class="btn btn-primary btn-sm">Opções do produto</a>
                <?php endif; ?>
                <a href="<?php echo BASE_URL.'products/create';?>" class="btn btn-success btn-sm">Novo produto</a>
            </div>
        </div>
        <div class="box-body">
            <table class="table table-responsive">
                <tr>
                    <th class="text-center">Nome</th>
                    <th class="text-center">Categoria</th>
                    <th class="text-center">Marca</th>
                    <th class="text-center">Estoque</th>
                    <th class="text-center">Preço de</th>
                    <th class="text-center">Preço por</th>
                    <th class="text-center">Visualizar</th>
                    <th style="width: 130px;" class="text-center">Ações</th>
                </tr>

                <?php foreach ($products as $product): ?>

                    <tr class="text-center">
                        <td>
                            <?php echo $product['name']; ?>
                        </td>
                        <td>
                            <?php echo $product['name_category']; ?>
                        </td>
                        <td>
                            <?php echo $product['name_brand']; ?>
                        </td>
                        <td>
                            <span style="color: <?php echo $product['stock'] > 0 ? 'green' : 'red' ?>; font-weight: bold;">
                                <?php echo $product['stock']; ?>
                            </span>
                        </td>
                        <td>
                            <span>R$ </span><?php echo number_format($product['price_from'],2, ',', '.'); ?>
                        </td>
                        <td>
                            <span>R$ </span><?php echo number_format($product['price'], 2, ',', '.'); ?>
                        </td>
                        <td>
                            <a href="<?php echo BASE_URL_ECOMMERCE.'product/open/'.$product['id'];?>"
                               class="btn bg-purple btn-xs" target="_blank">Ver na loja</a>
                        </td>

                        <td>
                            <div class="btn-group">
                                <a href="<?php echo BASE_URL.'products/update/'.$product['id'];?>"
                                   class="btn btn-primary btn-xs">Editar</a>
                                <a href="<?php echo BASE_URL.'products/delete/'.$product['id'];?>"
                                   class="btn btn-danger btn-xs">Excluir</a>
                            </div>
                        </td>
                    </tr>

                <?php endforeach; ?>
            </table>
        </div>
    </div>

</section>