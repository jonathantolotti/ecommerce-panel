<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Permissões
        <small>Gerenciamento do nível de acesso dos usuários</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo BASE_URL;?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="<?php echo BASE_URL;?>permissions"><i class="fa fa-unlock"></i>Permissões</a></li>
        <li class="active">Novo grupo</li>
    </ol>

</section>

<!-- Main content -->
<section class="content container-fluid">

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Novo grupo de permissão</h3>
            <div class="box-tools">

            </div>
        </div>
        <div class="box-body">
            <form method="POST" action="<?php echo BASE_URL;?>permissions/onCreate">
                <div class="form-group">
                    <label for="name">Nome:</label>
                    <input type="text" class="form-control" name="name" id="name" required>
                </div>

                <?php foreach ($permission_items as $permission): ?>
                    <div class="form-group">
                        <input type="checkbox" name="items[]" value="<?php echo $permission['id']; ?>" id="item-<?php echo $permission['id'];?>">
                        <label for="item-<?php echo $permission['id'];?>"><?php echo $permission['name']; ?></label>
                    </div>
                <?php endforeach; ?>

                <input type="submit" class="btn btn-success" value="Salvar">
                <input type="reset" class="btn btn-primary" value="Limpar">
            </form>
        </div>
    </div>

</section>