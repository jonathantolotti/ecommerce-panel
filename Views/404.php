<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Page Header
        <small>Optional description</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo BASE_URL;?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Página não encontrada</li>
    </ol>

</section>

<!-- Main content -->
<section class="content container-fluid">
    <h1>Ops!</h1><br>
    <h2>Página não encontrada =(</h2>
    <a href="<?php echo BASE_URL; ?>"><button class="btn btn-success btn-lg">Voltar para a
            home</button></a>
</section>
