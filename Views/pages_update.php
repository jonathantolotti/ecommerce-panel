<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Páginas estáticas
        <small>Editar página</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo BASE_URL;?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="<?php echo BASE_URL;?>pages"><i class="fa fa-unlock"></i>Páginas estáticas</a></li>
        <li class="active">Editar página</li>
    </ol>

</section>

<!-- Main content -->
<section class="content container-fluid">

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Editar  página - <?php echo $pageInfo['title']; ?></h3>
            <div class="box-tools">

            </div>
        </div>
        <div class="box-body">
            <form method="POST" action="<?php echo BASE_URL;?>pages/onUpdate/<?php echo $pageInfo['id']; ?>">
                <div class="form-group">
                    <label for="name">Título:</label>
                    <input type="text" class="form-control" name="title" id="title" value="<?php echo $pageInfo['title']; ?>" required>
                </div>
                <div class="form-group">
                    <label for="body">Corpo da página:</label>
                    <textarea id="body" name="body" required><?php echo $pageInfo['body']; ?></textarea>
                </div>

                <input type="submit" class="btn btn-success" value="Salvar">
                <input type="reset" class="btn btn-primary" value="Limpar">
            </form>
        </div>
    </div>

</section>

<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=yv7wti8c3qtcne0y5a0ez3pmh499q15lf11xjzi89l5k7pzt"></script>
<script type="text/javascript">
    tinymce.init({
        selector: '#body',
        height: 500,
        menubar: false,
        plugins:[
            'textcolor media image lists'
        ],
        toolbar: 'undo redo | formatselect | bold italic backcolor | media image | alignleft aligncenter alignright alignjustify |' +
            ' bulllist numlist | removeformat',
        automatic_upload:true,
        file_picker_types:'images',
        images_upload_url: '<?php echo BASE_URL; ?>pages/upload'

    });
</script>