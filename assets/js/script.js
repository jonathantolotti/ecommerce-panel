$(function () {
    $('.newimage').on('click', function (e) {
        e.preventDefault();

        $('.products_files_area').append('<input type="file" name="images[]" id="images">');

    });

    $('.product_image a').on('click', function(){
        $(this).parent().remove();
    });

    $('#colorPanel').on('change', function () {
        let color = $("#colorPanel").val();

        $.ajax({
            url: requestUrl+'configuration/updateColor',
            type:'POST',
            data:{colorPanel: color},
            success:function(){
                location.reload();
            }
        });

    });

});