<?php
namespace Models;

use \Core\Model;
use PDO;

class Pages extends Model
{
    /**
     * Retorna todas as páginas
     *
     * @return array
     */
    public function getPages()
    {
        $pages = [];

        $sql = "SELECT id, title FROM pages";
        $sql = $this->db->query($sql);

        if ($sql->rowCount() > 0) {
            $pages = $sql->fetchAll(PDO::FETCH_ASSOC);
        }

        return $pages;
    }

    /**
     * Retorna uma página específica, com base no ID informado
     *
     * @param $idPage
     * @return array|mixed
     */
    public function getPageById($idPage)
    {
        $page = [];

        $sql = "SELECT * FROM pages WHERE id = :idPage";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":idPage", $idPage);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $page = $sql->fetch(PDO::FETCH_ASSOC);
        }

        return $page;
    }

    /**
     * Adiciona uma nova página
     *
     * @param $title
     * @param $body
     */
    public function addPage($title, $body)
    {
        $sql = "INSERT INTO pages (title, body) VALUES (:title, :body)";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":title", $title);
        $sql->bindValue(":body", $body);
        $sql->execute();
    }

    /**
     * Altera uma página
     *
     * @param $idPage
     * @param $title
     * @param $body
     */
    public function update($idPage, $title, $body)
    {
        $sql = "UPDATE pages SET title = :title, body = :body WHERE id = :idPage";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":idPage", $idPage);
        $sql->bindValue(":title", $title);
        $sql->bindValue(":body", $body);
        $sql->execute();
    }

    /**
     * Deleta uma página
     *
     * @param $idPage
     */
    public function delete($idPage)
    {
        $sql = "DELETE FROM pages WHERE id = :idPage";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":idPage", $idPage);
        $sql->execute();
    }
}