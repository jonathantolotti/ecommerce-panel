<?php
namespace Models;

use \Core\Model;
use PDO;

class Brands extends Model
{
    /**
     * Retorna total de marcas e quantidade de produtos de cada marca
     *
     * @return array
     */
    public function getAllBrands()
    {
        $brands = [];

        $sql = "SELECT b.*,
                (SELECT COUNT(*) FROM products AS p WHERE p.id_brand = b.id) as totalProducts
                FROM brands AS b";
        $sql = $this->db->query($sql);

        if ($sql->rowCount() > 0) {
            $brands = $sql->fetchAll(PDO::FETCH_ASSOC);
        }

        return $brands;
    }

    public function getBrandInfo($idBrand)
    {
        $brandInfo = [];

        $sql = "SELECT * FROM brands WHERE id = :id";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":id", $idBrand);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $brandInfo = $sql->fetch(PDO::FETCH_ASSOC);
        }

        return $brandInfo;
    }

    /**
     * Adiciona uma nova marca
     *
     * @param $name
     */
    public function addBrand($name)
    {
        $sql = "INSERT INTO brands (name) VALUES (:name)";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":name", $name);
        $sql->execute();
    }

    /**
     * Altera a marca.
     *
     * @param $name
     * @param $idBrand
     */
    public function update($name, $idBrand)
    {
        $sql = "UPDATE brands SET name = :name WHERE id = :id";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":name", $name);
        $sql->bindValue(":id", $idBrand);
        $sql->execute();
    }

    /**
     * Deleta marca se não houver produtos atrelados a ela.
     *
     * @param $idBrand
     */
    public function delete($idBrand)
    {
        if (Brands::verifyHasProducts($idBrand)) {
            $sql = "DELETE FROM brands WHERE id = :id";
            $sql = $this->db->prepare($sql);
            $sql->bindValue(":id", $idBrand);
            $sql->execute();
        }
    }

    /**
     * Verifica se existem produtos na marca para poder deletar.
     *
     * @param $idBrand
     * @return bool
     */
    private function verifyHasProducts($idBrand)
    {
        $sql = "SELECT id FROM products WHERE id_brand = :id";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":id", $idBrand);
        $sql->execute();

        if ($sql->rowCount() === 0) {
            return true;
        }
        return false;
    }


}