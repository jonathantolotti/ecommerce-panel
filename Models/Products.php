<?php
namespace Models;

use \Core\Model;
use PDO;

class Products extends Model
{
    public function getAllProducts()
    {
        $products = [];

        $sql = "SELECT p.id, p.id_category, p.id_brand, p.name, p.stock, p.price, p.price_from, 
                c.name AS name_category, b.name AS name_brand 
                FROM products AS p
                LEFT JOIN categories AS c ON p.id_category = c.id
                LEFT JOIN brands AS b ON p.id_brand = b.id";
        $sql = $this->db->query($sql);

        if ($sql->rowCount() > 0) {
            $products = $sql->fetchAll(PDO::FETCH_ASSOC);
        }

        return $products;
    }

    public function addProduct($id_category,
                                $id_brand,
                                $name,
                                $description,
                                $stock,
                                $price_from,
                                $price,
                                $weight,
                                $width,
                                $height,
                                $length,
                                $diameter,
                                $featured,
                                $sale,
                                $bestseller,
                                $new_product,
                                $options,
                                $images
    )
    {
        if (
            !empty($id_category) &&
            !empty($id_brand) &&
            !empty($name) &&
            !empty($stock) &&
            !empty($price)
        ) {
            $optionsSelected = [];

            foreach ($options as $optk =>  $option) {
                if (!empty($option)) {
                    $optionsSelected[$optk] = $option;

                }
            }

            $optionsIds = implode(',', array_keys($optionsSelected));

            $sql = "INSERT INTO products 
                    (id_category, 
                     id_brand, 
                     name, 
                     description, 
                     stock, 
                     price, 
                     price_from, 
                     featured, 
                     sale, 
                     bestseller, 
                     new_product, 
                     options, 
                     weight, 
                     width, 
                     height, 
                     lenght, 
                     diameter) 
                     VALUES (:id_category,
                             :id_brand,
                             :name,
                             :description,
                             :stock,
                             :price,
                             :price_from,
                             :featured,
                             :sale,
                             :bestseller,
                             :new_product,
                             :options,
                             :weight,
                             :width,
                             :height,
                             :lenght,
                             :diameter)";

            $sql = $this->db->prepare($sql);

            $sql->bindValue(":id_category", $id_category);
            $sql->bindValue(":id_brand", $id_brand);
            $sql->bindValue(":name", $name);
            $sql->bindValue(":description", $description);
            $sql->bindValue(":stock", $stock);
            $sql->bindValue(":price", $price);
            $sql->bindValue(":price_from", $price_from);
            $sql->bindValue(":featured", $featured);
            $sql->bindValue(":sale", $sale);
            $sql->bindValue(":bestseller", $bestseller);
            $sql->bindValue(":new_product", $new_product);
            $sql->bindValue(":options", $optionsIds);
            $sql->bindValue(":weight", $weight);
            $sql->bindValue(":width", $width);
            $sql->bindValue(":height", $height);
            $sql->bindValue(":lenght", $length);
            $sql->bindValue(":diameter", $diameter);

            $sql->execute();

            $idLastProduct = $this->db->lastInsertId();

            foreach ($optionsSelected as $optk => $option) {
                $sql = "INSERT INTO products_options (id_product, id_option, p_value) VALUES (:id_products, :id_option, :p_value)";
                $sql = $this->db->prepare($sql);

                $sql->bindValue(":id_products", $idLastProduct);
                $sql->bindValue(":id_option", $optk);
                $sql->bindValue(":p_value", $option);

                $sql->execute();
            }

            $allowedTypes = [
                'image/jpeg',
                'image/jpg',
                'image/png'
            ];

            for ($q = 0; $q < count($images['name']); $q++) {
                $tmp_name = $images['tmp_name'][$q];
                $type = $images['type'][$q];

                if (in_array($type, $allowedTypes)) {
                    $this->addProductImage($idLastProduct, $tmp_name, $type);
                }

            }
        }
    }

    public function addProductImage($idProduct, $tmp_name, $type)
    {
        switch ($type) {
            case 'image/jpeg':
            case 'image/jpg':
                $originalImage = imagecreatefromjpeg($tmp_name);
                break;

            case 'image/png':
                $originalImage = imagecreatefrompng($tmp_name);
                break;
        }

        if (!empty($originalImage)) {
            $width = 460;
            $height = 400;
            $ratio = $width / $height;

            list($originalWidth, $originalHeight) = getimagesize($tmp_name);

            $originalRatio = $originalWidth / $originalHeight;

            if ($ratio > $originalRatio) {
                $imageWidth = $height * $originalRatio;
                $imageWidth = $height;
            } else {
                $imageHeight = $width / $originalRatio;
                $imageWidth = $width;
            }

            if ($imageWidth < $imageWidth) {
                $imageWidth = $width;
                $imageHeight = $imageWidth / $originalRatio;
            }

            if ($imageHeight < $height) {
                $imageHeight = $height;
                $imageWidth = $imageHeight * $originalRatio;
            }

            $x = 0;
            $y = 0;

            if ($imageWidth > $width) {
                $x = ($imageWidth - $width) / 2;
            }

            if ($imageHeight > $height) {
                $y = ($imageHeight - $height) / 2;
            }

            $image = imagecreatetruecolor($width, $height);
            imagecopyresampled($image, $originalImage, -$x, -$y,0, 0, $imageWidth, $imageHeight, $originalWidth, $originalHeight);

            $newName = md5(time().rand(0,999)).'.jpg';

            if ($_SERVER['REMOTE_ADDR'] == '192.168.0.17') {
                imagejpeg($image, '../ecommerce/media/products/'.$newName);
            } else {
                imagejpeg($image, '../loja.jonathantolotti.com.br/media/products/'.$newName);
            }

            $nameImage = 'https://loja.jonathantolotti.com.br/media/products/'.$newName;

            $sql = "INSERT INTO products_images (id_product, url) VALUES (:id_product, :url)";
            $sql = $this->db->prepare($sql);
            $sql->bindValue(":id_product", $idProduct);
            $sql->bindValue(":url", $nameImage);
            $sql->execute();
        }
    }

    public function getProductsInfo($idProduct)
    {
        $productInfo = [];

        $sql = "SELECT * FROM products WHERE id = :idProduct";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":idProduct", $idProduct);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $productInfo = $sql->fetch(PDO::FETCH_ASSOC);

            $sql = "SELECT id_option, p_value FROM products_options WHERE id_product = :idProduct";
            $sql = $this->db->prepare($sql);
            $sql->bindValue(":idProduct", $idProduct);
            $sql->execute();

            if ($sql->rowCount() > 0) {
                $options = $sql->fetchAll(PDO::FETCH_ASSOC);

                $productInfo['options'] = [];

                foreach ($options as $option) {
                    $productInfo['options'][$option['id_option']] = $option['p_value'];
                }
            }
        }

        $sql = "SELECT id, url FROM products_images WHERE id_product = :idProduct";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":idProduct", $idProduct);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $images = $sql->fetchAll(PDO::FETCH_ASSOC);

            $productInfo['images'] = [];

            foreach ($images as $image) {
                $productInfo['images'][$image['id']] = $image['url'];
            }
        }

        return $productInfo;
    }

    public function update($id_category,
                           $id_brand,
                           $name,
                           $description,
                           $stock,
                           $price_from,
                           $price,
                           $weight,
                           $width,
                           $height,
                           $length,
                           $diameter,
                           $featured,
                           $sale,
                           $bestseller,
                           $new_product,
                           $options,
                           $images,
                           $c_images,
                           $idProduct)
    {
        if (
            !empty($id_category) &&
            !empty($id_brand) &&
            !empty($name) &&
            !empty($stock) &&
            !empty($price) &&
            !empty($idProduct)
        ) {
            $optionsSelected = [];

            foreach ($options as $optk =>  $option) {
                if (!empty($option)) {
                    $optionsSelected[$optk] = $option;

                }
            }

            $optionsIds = implode(',', array_keys($optionsSelected));

            $sql = "UPDATE products SET id_brand = :id_brand, id_category = :id_category, name = :name, description = :description, stock = :stock,
                    price = :price, price_from = :price_from, featured = :featured, sale = :sale, bestseller = :bestseller,
                    new_product = :new_product, options = :options, weight = :weight, width = :width, height = :height, lenght = :lenght,
                    diameter = :diameter WHERE id = :idProduct";

            $sql = $this->db->prepare($sql);

            $sql->bindValue(":id_category", $id_category);
            $sql->bindValue(":id_brand", $id_brand);
            $sql->bindValue(":name", $name);
            $sql->bindValue(":description", $description);
            $sql->bindValue(":stock", $stock);
            $sql->bindValue(":price", $price);
            $sql->bindValue(":price_from", $price_from);
            $sql->bindValue(":featured", $featured);
            $sql->bindValue(":sale", $sale);
            $sql->bindValue(":bestseller", $bestseller);
            $sql->bindValue(":new_product", $new_product);
            $sql->bindValue(":options", $optionsIds);
            $sql->bindValue(":weight", $weight);
            $sql->bindValue(":width", $width);
            $sql->bindValue(":height", $height);
            $sql->bindValue(":lenght", $length);
            $sql->bindValue(":diameter", $diameter);
            $sql->bindValue(":idProduct", $idProduct);

            $sql->execute();

            $sql = "DELETE FROM products_options WHERE id_product = :idProduct";
            $sql = $this->db->prepare($sql);
            $sql->bindValue(":idProduct", $idProduct);
            $sql->execute();

            foreach ($optionsSelected as $optk => $option) {
                $sql = "INSERT INTO products_options (id_product, id_option, p_value) VALUES (:id_products, :id_option, :p_value)";
                $sql = $this->db->prepare($sql);

                $sql->bindValue(":id_products", $idProduct);
                $sql->bindValue(":id_option", $optk);
                $sql->bindValue(":p_value", $option);

                $sql->execute();
            }

            $allowedTypes = [
                'image/jpeg',
                'image/jpg',
                'image/png'
            ];

            if (is_array($c_images)) {
                foreach ($c_images as $ckey => $cimg) {
                    $c_images[$ckey] = intval($cimg);
                }

                $sql = "DELETE FROM products_images WHERE id_product = :idProduct AND id NOT IN (".implode(',', $c_images).")";
                $sql = $this->db->prepare($sql);
                $sql->bindValue(":idProduct", $idProduct);
                $sql->execute();
            }

            for ($q = 0; $q < count($images['name']); $q++) {
                $tmp_name = $images['tmp_name'][$q];
                $type = $images['type'][$q];

                if (in_array($type, $allowedTypes)) {
                    $this->addProductImage($idProduct, $tmp_name, $type);
                }

            }
        }
    }
}