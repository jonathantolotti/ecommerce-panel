<?php
namespace Models;

use \Core\Model;

class Users extends Model
{
    private $userId;
    private $permissions;
    private $userName;
    private $userGroup;
    private $userAdmin;
    private $colorPanel;

    /**
     * @return bool
     */
    public function isLogged()
    {
        if (!empty($_SESSION['token'])) {
            $token = $_SESSION['token'];

            $sql = "SELECT u.id, u.id_permission, u.name AS userName, u.admin, pg.name, u.color_panel FROM users AS u
                    INNER JOIN permission_groups AS pg ON u.id_permission = pg.id
                    WHERE token = :token";
            $sql = $this->db->prepare($sql);
            $sql->bindValue(":token", $token);
            $sql->execute();

            if ($sql->rowCount() > 0) {
                $permissions = new Permissions();

                $data = $sql->fetch();

                $this->userId = $data['id'];
                $this->userName = $data['userName'];
                $this->userGroup = $data['name'];
                $this->userAdmin = $data['admin'];
                $this->colorPanel = $data['color_panel'];

                $this->permissions = $permissions->getPermissions($data['id_permission']);

                return true;
            }
        }
        return false;
    }

    public function validateAuth($email, $password)
    {
        $sql = "SELECT id FROM users WHERE email = :email AND password = :pass AND admin = 1";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":email", $email);
        $sql->bindValue(":pass", $password);
        $sql ->execute();

        if ($sql->rowCount() > 0) {

            $data = $sql->fetch();
            $token = md5(time().rand(0,999).$this->userId.time());

            $sql = "UPDATE users SET token = :token WHERE id = :id";
            $sql = $this->db->prepare($sql);
            $sql->bindValue(":token", $token);
            $sql->bindValue(":id", $data['id']);
            $sql->execute();

            $_SESSION['token'] = $token;

            return true;
        }

        return false;
    }

    public function updateColor($userId, $color)
    {
        $sql = "UPDATE users SET color_panel = :color_panel WHERE id = :userId";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":color_panel", $color);
        $sql->bindValue(":userId", $userId);
        $sql->execute();
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return mixed
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @return mixed
     */
    public function getUserAdmin()
    {
        if ($this->userAdmin == '1') {
            return true;
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getUserGroup()
    {
        return $this->userGroup;
    }

    public function hasPermission($permissionSlug)
    {
        if (in_array($permissionSlug, $this->permissions)) {
            return true;
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getColorPanel()
    {
        return $this->colorPanel;
    }
}