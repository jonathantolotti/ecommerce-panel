<?php
namespace Models;

use \Core\Model;
use PDO;

class Logger extends Model
{
    public function create($url, $request, $userId, $type)
    {
        $sql = "INSERT INTO user_logs (url, request, user_id, type) VALUES (:url, :request, :user_id, :type)";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":url", $url);
        $sql->bindValue(":request", $request);
        $sql->bindValue(":user_id", $userId);
        $sql->bindValue(":type", $type);
        $sql->execute();
    }

    public function getLogs()
    {
        $logs = [];

        $sql = "SELECT ul.*, u.name FROM user_logs AS ul LEFT JOIN users AS u ON ul.user_id = u.id ORDER BY ul.date DESC LIMIT 100";
        $sql = $this->db->query($sql);

        if ($sql->rowCount() > 0) {
            $logs = $sql->fetchAll(PDO::FETCH_ASSOC);
        }

        return $logs;
    }

    public function getAllLogs()
    {
        $logs = [];

        $sql = "SELECT ul.*, u.name FROM user_logs AS ul LEFT JOIN users AS u ON ul.user_id = u.id ORDER BY ul.date DESC";
        $sql = $this->db->query($sql);

        if ($sql->rowCount() > 0) {
            $logs = $sql->fetchAll(PDO::FETCH_ASSOC);
        }

        return $logs;
    }
}