<?php
namespace Models;

use \Core\Model;
use PDO;

class Permissions extends Model
{
    public function getPermissionGroupName($idPermission)
    {
        $sql = "SELECT name FROM permission_groups WHERE id = :idPermission";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":idPermission", $idPermission);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $permissionName = $sql->fetch();

            return $permissionName['name'];
        }

        return '';
    }

    public function getPermissions($idPermission)
    {
        $permissions = [];

        $sql = "SELECT id_permission_item FROM permission_links WHERE id_permission_group = :idPermission";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":idPermission", $idPermission);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $data = $sql->fetchAll();
            $permissionIds = [];

            foreach ($data as $dataItems) {
                $permissionIds[] = $dataItems['id_permission_item'];
            }

            $sql = "SELECT slug FROM permission_items WHERE id IN (".implode(',', $permissionIds).")";
            $sql = $this->db->query($sql);

            if ($sql->rowCount() > 0) {
                $data = $sql->fetchAll();
                $permissions = [];

                foreach ($data as $data_item) {
                    $permissions[] = $data_item['slug'];
                }
            }
        }

        return $permissions;
    }

    public function getAllGroups()
    {
        $permissionsGroups = [];

        $sql = "SELECT permission_groups.*, 
                (SELECT count(users.id) 
                FROM users 
                WHERE users.id_permission = permission_groups.id) as totalUsers 
                FROM permission_groups";
        $sql = $this->db->query($sql);

        if ($sql->rowCount() > 0) {
            $permissionsGroups = $sql->fetchAll(PDO::FETCH_ASSOC);
        }

        return $permissionsGroups;
    }

    public function getAllPermissions()
    {
        $permissions = [];

        $sql = "SELECT * FROM permission_items";
        $sql = $this->db->query($sql);

        if ($sql->rowCount() > 0) {
            $permissions = $sql->fetchAll(PDO::FETCH_ASSOC);
        }

        return $permissions;
    }

    public function addPermissionItem($name, $slug)
    {
        $sql = "INSERT into permission_items (name, slug) VALUES (:name, :slug)";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":name", $name);
        $sql->bindValue(":slug", $slug);
        $sql->execute();
    }

    public function addGroup($name)
    {
        $sql = "INSERT INTO permission_groups (name) VALUES (:name)";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":name", $name);
        $sql->execute();

        return $this->db->lastInsertId();
    }

    public function linkItemToGroup($idItem, $idGroup)
    {
        $sql = "INSERT INTO permission_links (id_permission_group, id_permission_item) VALUES (:idGroup, :idItem)";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":idGroup", $idGroup);
        $sql->bindValue(":idItem", $idItem);
        $sql->execute();
    }

    public function deleteGroup($idGroup)
    {
        if (Permissions::verifyDeleteGroup($idGroup)) {

            $sql = "DELETE FROM permission_links WHERE id_permission_group = :idGroup";
            $sql = $this->db->prepare($sql);
            $sql->bindValue(":idGroup", $idGroup);
            $sql->execute();

            $sql = "DELETE FROM permission_groups WHERE id = :idGroup";
            $sql = $this->db->prepare($sql);
            $sql->bindValue(":idGroup", $idGroup);
            $sql->execute();

        }
    }

    public function clearLink($idGroup)
    {
        $sql = "DELETE FROM permission_links WHERE id_permission_group = :idGroup";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":idGroup", $idGroup);
        $sql->execute();
    }

    public function setName($name, $idGroup)
    {
        $sql = "UPDATE permission_groups SET name = :name WHERE id = :idGroup";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":name", $name);
        $sql->bindValue(":idGroup", $idGroup);
        $sql->execute();
    }

    private function verifyDeleteGroup($idGroup)
    {
        $sql = "SELECT id FROM users WHERE id_permission = :idGroup";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":idGroup", $idGroup);
        $sql->execute();

        if ($sql->rowCount() === 0) {
            return true;
        }
            return false;
    }
}