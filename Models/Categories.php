<?php
namespace Models;

use \Core\Model;
use PDO;

class Categories extends Model
{
    /**
     * Retorna todas as categorias e suas filhas
     *
     * @return array
     */
    public function getAllCategories()
    {
        $categories = [];

        $sql = "SELECT * FROM categories ORDER BY sub DESC";
        $sql = $this->db->query($sql);

        if ($sql->rowCount() > 0) {
            $data = $sql->fetchAll(PDO::FETCH_ASSOC);

            foreach ($data as $item) {
                $item['subs'] = [];
                $categories[$item['id']] = $item;
            }

            while ($this->stillNeed($categories)) {
                $this->organizeCategory($categories);
            }

        }

        return $categories;
    }

    /**
     * Adiciona uma nova categoria e sua filha
     *
     * @param $name
     * @param $sub
     */
    public function addCategory($name, $sub)
    {
        $sql = "INSERT INTO categories (name, sub) 
                VALUE (:name, :sub)";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":name", $name);
        $sql->bindValue(":sub", $sub);
        $sql->execute();
    }

    /**
     * Altera uma categoria
     *
     * @param $name
     * @param $sub
     * @param $idCategory
     */
    public function updateCategory($name, $sub, $idCategory)
    {
        $sql = "UPDATE categories SET name = :name, sub = :sub WHERE id = :idCategory";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":name", $name);
        $sql->bindValue(":sub", $sub);
        $sql->bindValue(":idCategory", $idCategory);
        $sql->execute();
    }

    /**
     * Retorna dados de uma categoria com base no ID informado
     *
     * @param $idCategory
     * @return array|mixed
     */
    public function getCategoryInfoById($idCategory)
    {
        $categoryInfo = [];

        $sql = "SELECT name, sub FROM categories WHERE id = :id";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":id", $idCategory);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $categoryInfo = $sql->fetch(PDO::FETCH_ASSOC);
        }

        return $categoryInfo;
    }

    /**
     * Verifica quais categorias são filhas para excluir e adiciona no array
     *
     * @param $idCategory
     * @param array $categories
     * @return array
     */
    public function scanCategory($idCategory, $categories = [])
    {
        if (!in_array($idCategory, $categories)) {
            $categories[] = $idCategory;
        }

        $sql = "SELECT id FROM categories WHERE sub = :id";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":id", $idCategory);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $data = $sql->fetchAll();

            foreach ($data as $item) {
                if (!in_array($item['id'], $categories)) {
                    $categories[] = $item['id'];
                }

                $categories = $this->scanCategory($item['id'], $categories);
            }
        }

        return $categories;
    }

    /**
     * Verifica se tem produtos nas categorias
     *
     * @param $idCategory
     * @return bool
     */
    public function hasProducts($idCategory)
    {
        $sql = "SELECT COUNT(*) AS c FROM products WHERE id_category IN (".implode(',', $idCategory).")";
        $sql = $this->db->query($sql);

        $data = $sql->fetch();

        if (intval($data['c']) > 0) {
            return true;
        }

        return false;
    }

    /**
     * Remove categoria pai e filhas
     *
     * @param $idCategory
     */
    public function deleteCategories($idCategory)
    {
        $sql = "DELETE FROM categories WHERE id IN (".implode(',', $idCategory).")";
        $sql = $this->db->query($sql);
    }

    /**
     * Verifica se ainda existe categoria filha para adicionar no array de getAllCategories
     *
     * @param $data
     * @return bool
     */
    private function stillNeed($data)
    {
        foreach ($data as $item) {
            if (!empty($item['sub'])) {
                return true;
            }
        }
        return false;
    }

    /**
     * Organiza as categorias filhas no array para exibição
     *
     * @param $data
     */
    private function organizeCategory(&$data)
    {
        foreach ($data as $id => $item) {
            if (!empty($item['sub'])) {
                $data[$item['sub']]['subs'][$item['id']] = $item;
                unset($data[$id]);
                break;
            }
        }
    }
}