<?php
namespace Models;

use \Core\Model;
use PDO;

class Options extends Model
{
    /**
     * Retorna total de marcas e quantidade de produtos de cada marca
     *
     * @param bool $hasProduct
     * @return array
     */
    public function getAllOptions($hasProduct = false)
    {
        $options = [];

        if ($hasProduct) {
            $sql = "SELECT *, 
                    (SELECT COUNT(*) FROM products_options 
                    WHERE products_options.id_option = options.id) AS totalProducts 
                    FROM options";
        } else {
            $sql = "SELECT * FROM options";
        }

        $sql = $this->db->query($sql);

        if ($sql->rowCount() > 0) {
            $options = $sql->fetchAll(PDO::FETCH_ASSOC);
        }

        return $options;
    }

    public function getOptionInfo($idOption)
    {
        $optionInfo = [];

        $sql = "SELECT * FROM options WHERE id = :id";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":id", $idOption);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $optionInfo = $sql->fetch(PDO::FETCH_ASSOC);
        }

        return $optionInfo;
    }

    /**
     * Adiciona uma nova opção
     *
     * @param $name
     */
    public function addOption($name)
    {
        $sql = "INSERT INTO options (name) VALUES (:name)";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":name", $name);
        $sql->execute();
    }

    /**
     * Altera a opção.
     *
     * @param $name
     * @param $idBrand
     */
    public function update($name, $idOption)
    {
        $sql = "UPDATE options SET name = :name WHERE id = :id";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":name", $name);
        $sql->bindValue(":id", $idOption);
        $sql->execute();
    }

    /**
     * Deleta marca se não houver produtos atrelados a ela.
     *
     * @param $idOption
     */
    public function delete($idOption)
    {
        if (Options::verifyHasProducts($idOption)) {
            $sql = "DELETE FROM options WHERE id = :id";
            $sql = $this->db->prepare($sql);
            $sql->bindValue(":id", $idOption);
            $sql->execute();
        }
    }

    /**
     * @param $idOption
     * @return bool
     */
    private function verifyHasProducts($idOption)
    {
        $sql = "SELECT id FROM products_options WHERE id_option = :id";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":id", $idOption);
        $sql->execute();

        if ($sql->rowCount() === 0) {
            return true;
        }
        return false;
    }
}