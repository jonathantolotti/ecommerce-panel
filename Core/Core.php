<?php

namespace Core;

class Core
{
    /**
     * Responsável por identificar os controllers, actions e args das funções
     */
    public function run()
    {
        $url = '/'.(isset($_GET['q'])?$_GET['q']:'');

		$params = ['NotFound'];
		if(!empty($url) && $url != '/') {
			$url = explode('/', $url);
			array_shift($url);

			$currentController = $url[0].'Controller';
			array_shift($url);

			if(isset($url[0]) && $url[0] != '/') {
				$currentAction = $url[0];
				array_shift($url);
			} else {
				$currentAction = 'index';
			}

			if(count($url) > 0) {
				$params = $url;
			}

		} else {
			$currentController = 'HomeController';
			$currentAction = 'index';
		}

		$currentController = ucfirst($currentController);
		$prefix = '\Controllers\\';

		if(!file_exists('Controllers/'.$currentController.'.php')) {
			$currentController = 'NotFoundController';
			$currentAction = 'index';
		}
        $newController = $prefix.$currentController;
		$c = new $newController();

		if(!method_exists($c, $currentAction)) {
			$currentAction = 'index';
		}

		call_user_func_array([$c, $currentAction], $params);

	}
}